"""
@name:     MessageBox.py
@autor:    Dor Eliyahu.
@brief:    Pop-up messages handling.
"""

from tkMessageBox import showerror
from constants import *


def error(message_content):
   showerror(ERROR, message_content)
