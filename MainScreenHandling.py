"""
@name:     MainScreen.py
@autor:    Dor Eliyahu.
@brief:    logic behind the main screen elements.
"""

from Tkinter import *
from MessageBox import *
from constants import *

NUMBERS = '0123456789'
account = []


def money_field_key_pressed(event):
    """
    @brief: 	Handle key pressed in the money field.
    @param: 	event   Data about the click event.
    """
    if event.keysym == 'Return':
        # enter pressed:   add_money()
        add_money(event.widget.get())
        event.widget.delete(0, 'end')


def add_money(text):
	"""
	@brief:		Add sum of money to the current account.
	"""
	if is_numbers_only(text):
		global account
		account.append(text)



def is_numbers_only(text):
	"""
	@brief:		Check if text contains numbers only.
	@param:		text	The text to check.
	@return:	True if contains numbers only, otherwise - False.
	"""
	for character in text: 
		if not character in NUMBERS:
			error(NON_NUMBER_ERROR)
			return False
	return True
