"""
@name:     Constants.py
@autor:    Dor Eliyahu.
@brief:    All words and sentences definitions.
"""

NON_NUMBER_ERROR = "You try to insert invalid number !"
ERROR = "Error"
