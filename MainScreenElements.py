"""
@name:     MainScreen.py
@autor:    Dor Eliyahu.
@brief:    Initialize main screen elements.
"""

from Tkinter import *
from MainScreenHandling import *


def init_welcome_label(frame):
    """
    @brief: Initialize welcome area.
    @param: Frame to init the area to.
    """
    welcome_label = Label(frame, text="Welcome to ELSOFT accountancy")
    welcome_label.pack(fill=X)


def init_money_field(frame):
    """
    @brief: Initialize money field frame.
    @param: Frame to init the field to.
    """
    money_field_label = Label(frame, text="Some Of Money: ")
    money_field_label.pack(side=LEFT)

    money_field_entry = Entry(frame)
    money_field_entry.bind("<Key>", money_field_key_pressed)
    money_field_entry.pack(side=LEFT)
