"""
@name:     MainScreen.py
@autor:    Dor Eliyahu.
@brief:    Script response on the main screen should appear.
"""

from MainScreenElements import *


def main():
    """
    @brief:    The entry point of the program.
    """
    root = Tk()
    
    init_welcome_label(root)

    init_money_field(root)

    root.mainloop()


if __name__ == "__main__":
    main()
